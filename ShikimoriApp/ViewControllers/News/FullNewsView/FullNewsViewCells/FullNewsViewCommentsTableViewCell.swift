//
//  FullNewsCommentsTableViewCell.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 24/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit

class FullNewsViewCommentsTableViewCell: UITableViewCell, LinkLabelInteractionDelegate {

    @IBOutlet weak var commentsAuthorAvatarImageView: UIImageView!
    @IBOutlet weak var commentsAuthorNicknameLabel: LinkLabel!
    @IBOutlet weak var commentsTimeLabel: UILabel!
    @IBOutlet weak var commentsMessageLabel: LinkLabel!
    
    @IBOutlet weak var commentsFooterSeparatorHeightConstraint: NSLayoutConstraint!
    
    var indexPath = IndexPath()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        commentsFooterSeparatorHeightConstraint.constant = 0.5
        commentsAuthorAvatarImageView.layer.cornerRadius = commentsAuthorAvatarImageView.frame.width / 2
        
        commentsAuthorNicknameLabel.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        commentsAuthorNicknameLabel.highlightedLinkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        
        commentsMessageLabel.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        commentsMessageLabel.highlightedLinkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        
        commentsAuthorNicknameLabel.interactionDelegate = self
        commentsMessageLabel.interactionDelegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func linkLabelDidSelectLink(linkLabel: LinkLabel, url: URL) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}
