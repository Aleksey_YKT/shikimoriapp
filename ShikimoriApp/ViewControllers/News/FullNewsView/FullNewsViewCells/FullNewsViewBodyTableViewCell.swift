//
//  FullNewsViewBodyTableViewCell.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 25/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit

class FullNewsViewBodyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fullNewsBodyLabel: LinkLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        fullNewsBodyLabel.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        fullNewsBodyLabel.highlightedLinkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
