//
//  FullNewsViewHeaderTableViewCell.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 25/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit

class FullNewsViewHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var fullNewsHeaderAuthorAvatar: UIImageView!
    @IBOutlet weak var fullNewsHeaderTitleTextView: UITextViewFixed!
    @IBOutlet weak var fullNewsHeaderAuthorAndLinkedTextView: UITextViewFixed!
    @IBOutlet weak var fullNewsHeaderTimeTextView: UITextViewFixed!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        fullNewsHeaderTitleTextView.layoutSubviews()
        fullNewsHeaderAuthorAndLinkedTextView.layoutSubviews()
        fullNewsHeaderTimeTextView.layoutSubviews()
        fullNewsHeaderAuthorAvatar.layer.cornerRadius = fullNewsHeaderAuthorAvatar.frame.width / 2
        
        fullNewsHeaderAuthorAndLinkedTextView.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
