//
//  FullNewsViewFooterTableViewCell.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 25/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit
import SDWebImage

class FullNewsViewFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var fullNewsFooterAttachmentsView: UIView!
    @IBOutlet weak var fullNewsFooterSourceTextView: UITextViewFixed!
    
    @IBOutlet weak var fullNewsFooterAttachmentsViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var fullNewsFooterTopSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var fullNewsFooterBottomSeparaitorHeightConstraint: NSLayoutConstraint!
    
    var attachmentsImages: [String] = []
    var attachmentsAreInitiated = false
    var attachmentsLoadedImages: [UIImage] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        fullNewsFooterTopSeparatorHeightConstraint.constant = 0.5
        fullNewsFooterBottomSeparaitorHeightConstraint.constant = 0.5
        fullNewsFooterSourceTextView.layoutSubviews()
        fullNewsFooterSourceTextView.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        DispatchQueue.main.async {
            if !self.attachmentsAreInitiated {
                if self.attachmentsImages.count != 0 {
                    self.initAttachmentsView()
                } else {
                    self.fullNewsFooterAttachmentsViewHeightConstraints.constant = 0
                }
            }
            
            self.attachmentsAreInitiated = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initAttachmentsView() {
        let attachmentsViewBound = fullNewsFooterAttachmentsView.bounds
        let attachmentsLayoutCGSizes: [[CGRect]] = [
            [
                CGRect(x: 2, y: 0, width: attachmentsViewBound.width - 4, height: attachmentsViewBound.height)
            ], [
                CGRect(x: 2, y: 0, width: attachmentsViewBound.width / 2 - 2, height: attachmentsViewBound.height),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height)
            ], [
                CGRect(x: 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2 - 2),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: attachmentsViewBound.height / 2 + 2, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2 - 2)
            ], [
                CGRect(x: 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2),
                CGRect(x: 2, y: attachmentsViewBound.height / 2, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: attachmentsViewBound.height / 2, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2)
            ], [
                CGRect(x: 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2 - 2),
                CGRect(x: 2, y: attachmentsViewBound.height / 2 + 2, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 2 - 2),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: 0, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 3 - 2),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: attachmentsViewBound.height / 3 + 2, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 3 - 4),
                CGRect(x: attachmentsViewBound.width / 2 + 2, y: attachmentsViewBound.height / 3 * 2 + 2, width: attachmentsViewBound.width / 2 - 4, height: attachmentsViewBound.height / 3 - 2)
            ], [
                CGRect(x: 2, y: 0, width: attachmentsViewBound.width / 2 - 2, height: attachmentsViewBound.height * 2 / 3 - 2),
                CGRect(x: attachmentsViewBound.width / 2 + 4, y: 0, width: attachmentsViewBound.width / 2 - 6, height: attachmentsViewBound.height * 2 / 3 - 2),
                CGRect(x: 2, y: attachmentsViewBound.height * 2 / 3 + 2, width: attachmentsViewBound.width / 4 - 2, height: attachmentsViewBound.height / 3 - 2),
                CGRect(x: attachmentsViewBound.width / 4 + 4, y: attachmentsViewBound.height * 2 / 3 + 2, width: attachmentsViewBound.width / 4 - 4, height: attachmentsViewBound.height / 3 - 2),
                CGRect(x: attachmentsViewBound.width * 2 / 4 + 4, y: attachmentsViewBound.height * 2 / 3 + 2, width: attachmentsViewBound.width / 4 - 4, height: attachmentsViewBound.height / 3 - 2),
                CGRect(x: attachmentsViewBound.width * 3 / 4 + 4, y: attachmentsViewBound.height * 2 / 3 + 2, width: attachmentsViewBound.width / 4 - 6, height: attachmentsViewBound.height / 3 - 2)
            ]
        ]
        
        for (i, attachmentsImage) in attachmentsImages.enumerated() {
            if i < 6 {
                if let attachmentLinkURL = URL(string: attachmentsImage) {
                    let imageView = UIImageView()
                    imageView.clipsToBounds = true
                    imageView.frame = attachmentsLayoutCGSizes[attachmentsImages.count <= 6 ? attachmentsImages.count - 1 : 5][i]

                    imageView.isUserInteractionEnabled = false
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.footerImageAttachmentTapped(_:)))
                    imageView.addGestureRecognizer(tap)
                    
                    imageView.contentMode = .scaleAspectFill
                    
                    imageView.sd_setImage(with: attachmentLinkURL, placeholderImage: nil, options: [.retryFailed], progress: nil) { (image, _, _, _) in
                        if let image = image {
                            self.attachmentsLoadedImages.append(image)
                            imageView.isUserInteractionEnabled = true
                        }
                    }
                    
                    fullNewsFooterAttachmentsView.insertSubview(imageView, at: i)
                    
                    if attachmentsImages.count > 6 && i == 5 {
                        let view = UIView()
                        view.frame = imageView.bounds
                        
                        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
                        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                        blurEffectView.frame = CGRect(x: view.bounds.minX - 1, y: view.bounds.minY - 1, width: view.bounds.width + 2, height: view.bounds.height + 2)
                        view.addSubview(blurEffectView)
                        
                        let label = UILabel()
                        label.frame = view.bounds
                        label.textAlignment = .center
                        label.font = UIFont.boldSystemFont(ofSize: 20)
                        label.textColor = .white
                        label.text = "+" + String(attachmentsImages.count - 5)
                        view.addSubview(label)
                        
                        imageView.addSubview(view)
                    }
                }
            } else {
                if let attachmentLinkURL = URL(string: attachmentsImage) {
                    SDWebImageManager.shared.loadImage(with: attachmentLinkURL, options: .retryFailed, progress: nil) { (image, _, _, _, _, _) in
                        if let image = image {
                            self.attachmentsLoadedImages.append(image)
                        }
                    }
                }
            }
        }
    }
    
    @objc func footerImageAttachmentTapped(_ sender: UITapGestureRecognizer) {
        if let imageView = sender.view as? UIImageView {
            if let image = imageView.image {
                if let selectedImage = attachmentsLoadedImages.first(where: {$0 == image}) {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fullNewsViewFooterAttachmentImageTapped"), object: nil, userInfo: ["selectedImage": selectedImage, "allImages": attachmentsLoadedImages])
                }
            }
        }
    }
    
}
