//
//  FullNewsViewVideoTableViewCell.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 02/10/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit

class FullNewsViewVideoTableViewCell: UITableViewCell {

    @IBOutlet weak var fullNewsVideoPreviewImageView: UIImageView!
    @IBOutlet weak var fullNewsVideoLinkedView: UIView!
    @IBOutlet weak var fullNewsVideoPreviewImageViewHeightConstraint: NSLayoutConstraint!
    
    var videoLink = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let videoLinkedViewTap = UITapGestureRecognizer(target: self, action: #selector(self.videoLinkedViewTapped))
        fullNewsVideoLinkedView.addGestureRecognizer(videoLinkedViewTap)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func videoLinkedViewTapped(sender: UITapGestureRecognizer) {
        if let videoLinkURL = URL(string: videoLink) {
            UIApplication.shared.open(videoLinkURL, options: [:], completionHandler: nil)
        }
    }

}
