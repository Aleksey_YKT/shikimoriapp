//
//  FullNewsViewController.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 24/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit
import SDWebImage

class FullNewsViewController: UIViewController {
        
    @IBOutlet weak var fullNewsTableView: UITableView!
    
    var selectedNewsTopicRealm = TopicRealm()
    var newsIsLoaded = false
    var needToHighlightCommentCell: IndexPath? = nil
    var searchingForCommentWithId: Int? = nil
    
    enum FullNewsViewCells: Int {
        case header = 0
        case body = 1
        case video = 2
        case footer = 3
        case comments = 4
        
        case sectionsCount = 5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initFullNewsViewNotificationObservers()
        
        currentCommentsPage = 1
        initFullNewsViewModel(selectedNewsTopicRealm)
        getNewsTopicComments(page: currentCommentsPage, commentableId: selectedNewsTopicRealm.id)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        clearFullNewsViewModel()
    }
}

extension FullNewsViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if newsIsLoaded {
            return FullNewsViewCells.sectionsCount.rawValue
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if newsIsLoaded {
            switch section {
            case FullNewsViewCells.header.rawValue:
                return 1
            case FullNewsViewCells.body.rawValue:
                return 1
            case FullNewsViewCells.video.rawValue:
                return fullNewsModel.footerAttachmentsVideos.count
            case FullNewsViewCells.footer.rawValue:
                return 1
            case FullNewsViewCells.comments.rawValue:
                return fullNewsCommentsModel.count
            default:
                return 0
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case FullNewsViewCells.header.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fullNewsViewHeaderTableViewCell", for: indexPath) as! FullNewsViewHeaderTableViewCell
            
            cell.fullNewsHeaderTitleTextView.text = fullNewsModel.headerTitle
            if let userAvatarURL = URL(string: fullNewsModel.headerAuthorAvatar) {
                cell.fullNewsHeaderAuthorAvatar.sd_setImage(with: userAvatarURL)
            }
            cell.fullNewsHeaderTimeTextView.text = fullNewsModel.headerCreatedDate
            cell.fullNewsHeaderAuthorAndLinkedTextView.attributedText = fullNewsModel.headerAuthorAndLinked
            
            return cell
        case FullNewsViewCells.body.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fullNewsViewBodyTableViewCell", for: indexPath) as! FullNewsViewBodyTableViewCell
            
            cell.fullNewsBodyLabel.attributedText = fullNewsModel.body
            
            return cell
        case FullNewsViewCells.video.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fullNewsViewVideoTableViewCell", for: indexPath) as! FullNewsViewVideoTableViewCell
            
            if let previewURL = URL(string: fullNewsModel.footerAttachmentsVideos[indexPath.row].preview) {
                cell.fullNewsVideoPreviewImageView.sd_setImage(with: previewURL)
//                { (previewImage, _, _, _) in
//                    if let previewImage = previewImage {
//                        if previewImage.size.width > tableView.bounds.width {
//                            let previewImageAspectRatio = previewImage.size.width / tableView.bounds.width
//                            cell.fullNewsVideoPreviewImageViewHeightConstraint.constant = previewImage.size.height / previewImageAspectRatio
//                        } else {
//                            cell.fullNewsVideoPreviewImageViewHeightConstraint.constant = previewImage.size.height
//                        }
//                    }
//                }
            }
            
            cell.videoLink = fullNewsModel.footerAttachmentsVideos[indexPath.row].link
            
            return cell
        case FullNewsViewCells.footer.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fullNewsViewFooterTableViewCell", for: indexPath) as! FullNewsViewFooterTableViewCell
            
            cell.attachmentsImages = fullNewsModel.footerAttachmentsImages
            cell.fullNewsFooterSourceTextView.attributedText = fullNewsModel.footerSource
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "fullNewsViewCommentsTableViewCell", for: indexPath) as! FullNewsViewCommentsTableViewCell
            
            cell.indexPath = indexPath
            
            if let authorAvatarURL = URL(string: fullNewsCommentsModel[indexPath.row].authorAvatar) {
                cell.commentsAuthorAvatarImageView.sd_setImage(with: authorAvatarURL, completed: nil)
            }
            
            cell.commentsAuthorNicknameLabel.attributedText = fullNewsCommentsModel[indexPath.row].authorNickname
            cell.commentsTimeLabel.text = fullNewsCommentsModel[indexPath.row].createdDate
            cell.commentsMessageLabel.attributedText = fullNewsCommentsModel[indexPath.row].comment
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let _ = cell as? FullNewsViewCommentsTableViewCell {
            if indexPath.row + 1 == fullNewsCommentsModel.count {
                if newsCommentsHasNextPage {
                    currentCommentsPage += 1
                    getNewsTopicComments(page: currentCommentsPage, commentableId: selectedNewsTopicRealm.id)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: false)
        if let _ = tableView.cellForRow(at: indexPath) as? FullNewsViewCommentsTableViewCell {
            print(fullNewsCommentsRealm[indexPath.row].body)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let needToHiglightCellIndex = needToHighlightCommentCell {
            if let cells = fullNewsTableView.visibleCells as? [FullNewsViewCommentsTableViewCell] {
                if let cell = cells.first(where: {$0.indexPath == needToHiglightCellIndex}) {
                    cell.setHighlighted(true, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        cell.setHighlighted(false, animated: true)
                    }

                    needToHighlightCommentCell = nil
                }
            }
        }
    }
}

extension FullNewsViewController {
    private func initFullNewsViewNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(newsDidUpdate(_:)), name: Notification.Name("fullNewsViewModelIsLoaded"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToComment(_:)), name: Notification.Name("scrollToCommentInFullNewsView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(fullNewsFooterAttachmentImageTapped(_:)), name: Notification.Name("fullNewsViewFooterAttachmentImageTapped"), object: nil)
    }
    
    @objc func newsDidUpdate(_ notification: NSNotification) {
        newsIsLoaded = true
        fullNewsTableView.reloadData()
        
        DispatchQueue.main.async {
            if let searchingForComment = self.searchingForCommentWithId {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "scrollToCommentInFullNewsView"), object: nil, userInfo: ["commentId": searchingForComment])
                self.searchingForCommentWithId = nil
            }
        }
    }
    
    @objc func scrollToComment(_ notification: NSNotification) {
        if let commentId = notification.userInfo?["commentId"] as? Int {
            if let commentIndex = fullNewsCommentsModel.firstIndex(where: {$0.id == commentId}) {
                let commentCellIndexPath = IndexPath(row: commentIndex, section: FullNewsViewCells.comments.rawValue)
                fullNewsTableView.contentOffset = CGPoint(x: fullNewsTableView.contentOffset.x, y: fullNewsTableView.contentOffset.y - 1)
                fullNewsTableView.scrollToRow(at: commentCellIndexPath, at: .top, animated: true)
                needToHighlightCommentCell = commentCellIndexPath
                searchingForCommentWithId = nil
            } else {
                if newsCommentsHasNextPage {
                    searchingForCommentWithId = commentId
                    currentCommentsPage += 1
                    getNewsTopicComments(page: currentCommentsPage, commentableId: selectedNewsTopicRealm.id)
                } else {
                    print("comment doesn't exist")
                }
            }
        }
    }
    
    @objc func fullNewsFooterAttachmentImageTapped(_ notification: NSNotification) {
        if let selectedImage = notification.userInfo?["selectedImage"] as? UIImage, let allImages = notification.userInfo?["allImages"] as? [UIImage] {
            let accessoriesStoryboard = UIStoryboard(name: "AccessoryScreens", bundle: nil)
            if let imageViewer = accessoriesStoryboard.instantiateViewController(withIdentifier: "imageViewer") as? ImageViewerViewController {                
                imageViewer.images = allImages
                imageViewer.selectedImage = selectedImage
                
                let navigationController = UINavigationController()
                navigationController.addChild(imageViewer)
                navigationController.navigationBar.isTranslucent = true
                
                self.present(navigationController, animated: true, completion: nil)
            }
        }
    }
    
}
