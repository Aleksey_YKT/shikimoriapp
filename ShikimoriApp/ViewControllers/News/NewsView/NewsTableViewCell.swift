//
//  NewsTableViewCell.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit
import WebKit

class NewsTableViewCell: UITableViewCell, UITextViewDelegate, LinkLabelInteractionDelegate {

    @IBOutlet weak var newsHeadersView: UIView!
    @IBOutlet weak var newsBodyView: UIView!
    
    @IBOutlet weak var newsHeaderUserAvatarImageView: UIImageView!
    @IBOutlet weak var newsHeaderTitleTextView: UITextViewFixed!
    @IBOutlet weak var newsHeaderAuthorAndLinkedTextView: UITextViewFixed!
    @IBOutlet weak var newsHeaderCreatedAtTextView: UITextViewFixed!
    
    @IBOutlet weak var newsBodyLabel: LinkLabel!
    @IBOutlet weak var newsBodyExtendButton: UIButton!
    @IBOutlet weak var newsBodyExtendButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var newsFooterImageView: UIImageView!
    @IBOutlet weak var newsFooterImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsFooterLinkedView: UIView!
    
    @IBOutlet weak var newsFooterCommentsCountLabel: UILabel!
    @IBOutlet weak var newsFooterSourceTextView: UITextViewFixed!
    
    @IBOutlet weak var newsTableTopSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsTableFooterSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsTableBottonSeparatorHeightConstraint: NSLayoutConstraint!
    
    var newsTableViewCellRow = 0
    var newsFooterLink: URL? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        newsHeaderTitleTextView.textContainer.lineBreakMode = NSLineBreakMode.byWordWrapping
        newsHeaderAuthorAndLinkedTextView.textContainer.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        newsHeaderUserAvatarImageView.layer.cornerRadius = newsHeaderUserAvatarImageView.frame.width / 2
        
        newsTableTopSeparatorHeightConstraint.constant = 0.5
        newsTableFooterSeparatorHeightConstraint.constant = 0.5
        newsTableBottonSeparatorHeightConstraint.constant = 0.5
        
        newsHeaderAuthorAndLinkedTextView.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        newsBodyLabel.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        newsBodyLabel.highlightedLinkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        newsFooterSourceTextView.linkTextAttributes = [.foregroundColor: UIColor(named: "LinkedTextColor") ?? UIColor(red: 21/255, green: 76/255, blue: 129/255, alpha: 1.0)]
        
        newsHeaderAuthorAndLinkedTextView.delegate = self
        newsBodyLabel.interactionDelegate = self
        
        let footerLinkedViewTap = UITapGestureRecognizer(target: self, action: #selector(self.footerLinkedViewTapped))
        newsFooterLinkedView.addGestureRecognizer(footerLinkedViewTap)
        
        layoutUpdates()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        newsTableViewCellRow = 0
    }
    
    @IBAction func newsBodyExtendButtonPressed(_ sender: Any) {
        expandNewsBodyLabel()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tableCellDidChange"), object: nil, userInfo: ["newsTableViewCellRow": newsTableViewCellRow, "action": "newsBodyLabelDidExpanded"])
    }
    
    func expandNewsBodyLabel() {
        newsBodyLabel.numberOfLines = 0
        hideNewsBodyExtendButton()
    }
    
    func hideNewsBodyExtendButton() {
        newsBodyExtendButton.isHidden = true
        newsBodyExtendButtonHeightConstraint.constant = 0
    }
    
    func showNewsBodyExtendButton() {
        newsBodyExtendButton.isHidden = false
        newsBodyExtendButtonHeightConstraint.constant = 24
    }
    
    func layoutUpdates() {
        newsHeaderTitleTextView.layoutSubviews()
        newsHeaderAuthorAndLinkedTextView.layoutSubviews()
        newsHeaderCreatedAtTextView.layoutSubviews()
        newsFooterSourceTextView.layoutSubviews()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print(URL)
        
        return false
    }
    
    func linkLabelDidSelectLink(linkLabel: LinkLabel, url: URL) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func footerLinkedViewTapped(sender: UITapGestureRecognizer) {
        if let newsFooterLink = newsFooterLink {
            UIApplication.shared.open(newsFooterLink, options: [:], completionHandler: nil)
        }
    }
}
