//
//  ViewController.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit
import SDWebImage

class NewsTableViewController: UITableViewController {
    
    var newsIsLoaded = false
    var newsIsLoadingMore = false
    
    var expandedNewsBodyLabelsRow: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 250.0
        tableView.rowHeight = UITableView.automaticDimension
        
        initNewsTableViewNotificationCenterObservers()
        
        refreshControl?.addTarget(self, action: #selector(refreshNews(_:)), for: .valueChanged)
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.tintColor = UIColor(named: "PlainTextColor") ?? UIColor.darkText
        
        getNewsTopics(page: currentNewsPage)
    }
    
    @objc func refreshNews(_ notification: NSNotification) {
        newsIsLoadingMore = false
        newsIsLoaded = false
        clearNewsTableViewVariables()
        getNewsTopics(page: 1)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if newsIsLoaded {
            return newsTableViewModel.count
        } else {
            return 3
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsTableViewCell", for: indexPath) as! NewsTableViewCell

        if newsIsLoaded {
            let row = indexPath.row
            cell.newsTableViewCellRow = row
            
            if let authorAvatarURL = URL(string: newsTableViewModel[row].headerAuthorAvatar) {
                cell.newsHeaderUserAvatarImageView.sd_setImage(with: authorAvatarURL, placeholderImage: nil)
            }
            
            cell.newsHeaderTitleTextView.text = newsTableViewModel[row].headerTitle
            cell.newsHeaderAuthorAndLinkedTextView.attributedText = newsTableViewModel[row].headerAuthorAndLinked
            cell.newsHeaderCreatedAtTextView.text = newsTableViewModel[row].headerCreatedDate
            
            cell.newsBodyLabel.attributedText = newsTableViewModel[row].body
            
            if cell.newsBodyLabel.calculateMaxLines <= 5 && !expandedNewsBodyLabelsRow.contains(row) {
                cell.hideNewsBodyExtendButton()
            } else if expandedNewsBodyLabelsRow.contains(row) {
                cell.expandNewsBodyLabel()
            } else {
                cell.newsBodyLabel.numberOfLines = 5
                cell.showNewsBodyExtendButton()
            }
            
            if let footerImage = newsTableViewModel[row].footerImage {
                cell.newsFooterImageView.image = footerImage
                if footerImage.size.width > tableView.bounds.width {
                    let footerImageAspectRatio = footerImage.size.width / tableView.bounds.width
                    cell.newsFooterImageHeightConstraint.constant = footerImage.size.height / footerImageAspectRatio
                    cell.newsFooterImageHeightConstraint.priority = UILayoutPriority(999)
                } else {
                    cell.newsFooterImageHeightConstraint.constant = footerImage.size.height
                    cell.newsFooterImageHeightConstraint.priority = UILayoutPriority(999)
                }
            } else {
                cell.newsFooterImageView.image = nil
                cell.newsFooterImageHeightConstraint.constant = 0.0
                cell.newsFooterImageHeightConstraint.priority = UILayoutPriority(999)
            }
            
            if let footerLinkURL = URL(string: newsTableViewModel[row].footerLink) {
                cell.newsFooterLink = footerLinkURL
                cell.newsFooterLinkedView.isHidden = false
            } else {
                cell.newsFooterLink = nil
                cell.newsFooterLinkedView.isHidden = true
            }
            
            cell.newsFooterCommentsCountLabel.text = newsTableViewModel[row].footerCommentsCount
            
            if let newsSource = newsTableViewModel[row].footerSource {
                cell.newsFooterSourceTextView.attributedText = newsSource
            } else {
                cell.newsFooterSourceTextView.text = ""
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {        
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if newsIsLoaded {
            self.performSegue(withIdentifier: "showFullNewsSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFullNewsSegue" {
            if let viewController = segue.destination as? FullNewsViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    viewController.selectedNewsTopicRealm = newsTopicsRealm[indexPath.row]
                    tableView.deselectRow(at: indexPath, animated: false)
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 >= newsTableViewModel.count {
            if !newsIsLoadingMore && newsIsLoaded {
                currentNewsPage += 1
                getNewsTopics(page: currentNewsPage)
                newsIsLoadingMore = true
            }
        }
    }
}

extension NewsTableViewController {
    private func initNewsTableViewNotificationCenterObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(newsDidUpdate(_:)), name: Notification.Name("newsViewModelLoaded"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(tableCellDidChange(_:)), name: Notification.Name("tableCellDidChange"), object: nil)
    }
    
    @objc func newsDidUpdate(_ notification: NSNotification) {
        self.newsIsLoaded = true
        if self.newsIsLoadingMore == true {
            self.newsIsLoadingMore = false
        }
        self.tableView.reloadData()
        self.tableView.refreshControl?.endRefreshing()
    }
    
    @objc func tableCellDidChange(_ notification: NSNotification) {
        DispatchQueue.main.async {
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                 
                if let newsTableViewCell = notification.userInfo?["newsTableViewCellRow"] as? Int {
                    if let action = notification.userInfo?["action"] as? String {
                        switch action {
                        case "newsBodyLabelDidExpanded":
                            self.expandedNewsBodyLabelsRow.append(newsTableViewCell)
                        default:
                            return
                        }
                    }
                }
                 
                self.tableView.endUpdates()
             }
        }
    }
}

extension NewsTableViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if navigationController.viewControllers.count > 1 {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        } else {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
    }
}


extension UILabel {
    var calculateMaxLines: Int {
        if let labelFont = font {
            let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
            let charSize = labelFont.lineHeight
            let text = (self.text ?? "") as NSString
            let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: labelFont], context: nil)
            let lines = Int(textSize.height/charSize)
            
            return lines
        } else {
            return 5
        }
    }
}
