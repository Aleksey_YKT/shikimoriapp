//
//  ImageViewerViewController.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 29/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit

class ImageViewerViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageViewerScrollView: UIScrollView!
    
    var images: [UIImage] = []
    var selectedImage: UIImage? = nil
    
    var imageViews: [UIImageView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageViewerScrollView.delegate = self
        
        let blurEffect = UIBlurEffect(style: .regular)
        let blurBackgroundView = UIVisualEffectView(effect: blurEffect)
        blurBackgroundView.frame = self.view.bounds
        view.insertSubview(blurBackgroundView, belowSubview: imageViewerScrollView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        loadImages()
    }

    func loadImages() {
        let scrollViewWidth = self.imageViewerScrollView.bounds.width
        let scrollViewHeight = self.imageViewerScrollView.bounds.height
        
        for (i, imageWithLink) in images.enumerated() {
            let imageView = UIImageView(frame: CGRect(x: scrollViewWidth * CGFloat(i), y: 0, width: scrollViewWidth, height: scrollViewHeight))
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            imageView.image = imageWithLink
            
            self.imageViewerScrollView.addSubview(imageView)
            imageViews.append(imageView)
        }
        
        self.imageViewerScrollView.contentSize = CGSize(width: scrollViewWidth * CGFloat(images.count), height: scrollViewHeight)
        
        if let selectedImage = selectedImage {
            if let selectedImageIndex = images.firstIndex(where: {$0 == selectedImage}) {
                self.imageViewerScrollView.contentOffset = CGPoint(x: scrollViewWidth * CGFloat(selectedImageIndex), y: 0)
                
                self.title = String(selectedImageIndex + 1) + " из " + String(images.count)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.width
        let currentPage = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        self.title = String(currentPage + 1) + " из " + String(images.count)
    }
}
