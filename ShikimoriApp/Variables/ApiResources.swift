//
//  ApiResources.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation

let appURL = "shikiapp://"
let serverURL = "https://shikimori.one"

let getAccessTokenURL = serverURL + "/oath/token"

let getTopicsURL = serverURL + "/api/topics"
let getTopicComments = serverURL + "/api/comments"
