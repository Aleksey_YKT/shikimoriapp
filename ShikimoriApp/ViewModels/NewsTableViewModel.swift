//
//  NewsTableViewModel.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 16/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit
import SDWebImage

class NewsTabelViewModel {
    var headerTitle = ""
    var headerAuthorAndLinked: NSMutableAttributedString = NSMutableAttributedString()
    var headerAuthorAvatar = ""
    var headerCreatedDate = ""
    var body: NSMutableAttributedString = NSMutableAttributedString()
    var footerImage: UIImage? = nil
    var footerLink: String = ""
    var footerCommentsCount = ""
    var footerSource: NSAttributedString? = nil
}

var newsTopicsRealm: [TopicRealm] = []
var newsForumsRealm: [ForumRealm] = []
var newsUsersRealm: [UserRealm] = []
var newsLinkedsRealm: [LinkedRealm] = []

var newsTableViewModel: [NewsTabelViewModel] = []
private var loadedImagesCount = 0

func initNewsTableViewModel(_ newsTopicRealm: TopicRealm) {
    let font = UIFont.systemFont(ofSize: 14)
    let plainTextStyle: [NSAttributedString.Key: Any] = [
        .font: font,
        .foregroundColor: UIColor(named: "PlainTextColor") ?? UIColor.black,
    ]

    var linkTextStyle: [NSAttributedString.Key: Any] = [
        .font: font,
    ]

    let newsTableViewData = NewsTabelViewModel()
    newsTableViewData.headerTitle = newsTopicRealm.topicTitle
    newsTableViewData.headerCreatedDate = formateCreatedAtDateToString(newsTopicRealm.createdAt)

    if let user = newsUsersRealm.first(where: {$0.id == newsTopicRealm.userId}) {
        newsTableViewData.headerAuthorAndLinked.append(NSAttributedString(string: "от ", attributes: plainTextStyle))
        linkTextStyle[.link] = "shikiapp://user/" + String(user.id)
        newsTableViewData.headerAuthorAndLinked.append(NSAttributedString(string: user.nickname, attributes: linkTextStyle))

        newsTableViewData.headerAuthorAvatar = user.avatar
    }

    if let linked = newsLinkedsRealm.first(where: {$0.id == newsTopicRealm.linkedId}) {
        if linked.russian != "" {
            newsTableViewData.headerAuthorAndLinked.append(NSAttributedString(string: " о ", attributes: plainTextStyle))
            linkTextStyle[.link] = "shikiapp://linked/" + String(linked.id)
            newsTableViewData.headerAuthorAndLinked.append(NSAttributedString(string: linked.russian, attributes: linkTextStyle))
        } else if linked.name != "" {
            newsTableViewData.headerAuthorAndLinked.append(NSAttributedString(string: " о ", attributes: plainTextStyle))
            linkTextStyle[.link] = "shikiapp://linked/" + String(linked.id)
            newsTableViewData.headerAuthorAndLinked.append(NSAttributedString(string: linked.name, attributes: linkTextStyle))
        }
    }

    var clearBody = newsTopicRealm.body
    if let newsSource = newsTopicRealm.body.slice(from: "[source]", to: "[/source]") {
        if let newsSourceHost = URL(string: newsSource)?.host {
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.right

            linkTextStyle[.link] = newsSource
            linkTextStyle[.paragraphStyle] = style
            newsTableViewData.footerSource = NSAttributedString(string: newsSourceHost, attributes: linkTextStyle)
        }
        
        clearBody = String(clearBody.dropLast(("[source]" + newsSource + "[/source]").count))
    }
    
    let attributedNewsTopicBody = NSMutableAttributedString(string: clearBody, attributes: [.font: UIFont.systemFont(ofSize: 16)])
    
    let formattedBody = attributedNewsTopicBody.formateBBCodedString()
    newsTableViewData.body.append(formattedBody)
    
    if let footerImage = newsTopicRealm.footerImages.first {
        if let footerImageURL = URL(string: footerImage) {
            SDWebImageManager.shared.loadImage(with: footerImageURL, options: [.retryFailed], context: nil, progress: nil, completed: { (image, _, _, _, _, _) in
                newsTableViewData.footerImage = image
                loadedImagesCount += 1
                checkForLoadingsIsComplieted()
            })
        }
    } else {
        if let footerVideoPreview = newsTopicRealm.footerVideos.first {
            if let footerPreviewURL = URL(string: footerVideoPreview.preview) {
                SDWebImageManager.shared.loadImage(with: footerPreviewURL, options: [.retryFailed], context: nil, progress: nil, completed: { (image, _, _, _, _, _) in
                    newsTableViewData.footerImage = image
                    newsTableViewData.footerLink = footerVideoPreview.link
                    loadedImagesCount += 1
                    checkForLoadingsIsComplieted()
                })
            }
        } else {
            loadedImagesCount += 1
        }
    }
    
    newsTableViewData.footerCommentsCount = String(newsTopicRealm.commentsCount)

    newsTableViewModel.append(newsTableViewData)
    checkForLoadingsIsComplieted()
}

func clearNewsTableViewVariables() {
    newsTopicsRealm = []
    newsForumsRealm = []
    newsUsersRealm = []
    newsLinkedsRealm = []

    newsTableViewModel = []
    loadedImagesCount = 0
    recievedNewsTopicsCount = 0
}

private func checkForLoadingsIsComplieted() {
    if (loadedImagesCount == recievedNewsTopicsCount || currentNewsPage == 1) && newsTableViewModel.count == recievedNewsTopicsCount {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newsViewModelLoaded"), object: nil)
    }
}
