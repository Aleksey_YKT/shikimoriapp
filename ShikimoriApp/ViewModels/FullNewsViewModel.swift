//
//  FullNewsViewModel.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 25/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit
import SDWebImage

class FullNewsViewModel {
    var headerTitle = ""
    var headerAuthorAndLinked: NSMutableAttributedString = NSMutableAttributedString()
    var headerCreatedDate = ""
    var headerAuthorAvatar = ""
    var body: NSMutableAttributedString = NSMutableAttributedString()
    var footerAttachmentsImages: [String] = []
    var footerAttachmentsVideos: [TopicVideoRealm] = []
    var footerSource: NSAttributedString? = nil
}

class FullNewsViewCommentModel {
    var id = 0
    var authorAvatar = ""
    var authorNickname: NSMutableAttributedString = NSMutableAttributedString()
    var createdDate = ""
    var comment: NSMutableAttributedString = NSMutableAttributedString()
}

var fullNewsCommentsRealm: [CommentRealm] = []
var fullNewsModel = FullNewsViewModel()
var fullNewsCommentsModel: [FullNewsViewCommentModel] = []

func initFullNewsViewModel(_ newsTopicRealm: TopicRealm) {
    let font = UIFont.systemFont(ofSize: 16)
    let plainTextStyle: [NSAttributedString.Key: Any] = [
        .font: font,
        .foregroundColor: UIColor(named: "PlainTextColor") ?? UIColor.black,
    ]

    var linkTextStyle: [NSAttributedString.Key: Any] = [
        .font: font,
    ]
    
    fullNewsModel.headerTitle = newsTopicRealm.topicTitle
    fullNewsModel.headerCreatedDate = formateCreatedAtDateToString(newsTopicRealm.createdAt)

    if let user = newsUsersRealm.first(where: {$0.id == newsTopicRealm.userId}) {
        fullNewsModel.headerAuthorAndLinked.append(NSAttributedString(string: "от ", attributes: plainTextStyle))
        linkTextStyle[.link] = "shikiapp://author/" + String(user.id)
        fullNewsModel.headerAuthorAndLinked.append(NSAttributedString(string: user.nickname, attributes: linkTextStyle))

        fullNewsModel.headerAuthorAvatar = user.avatar
    }
    
    if let linked = newsLinkedsRealm.first(where: {$0.id == newsTopicRealm.linkedId}) {
        if linked.russian != "" {
            fullNewsModel.headerAuthorAndLinked.append(NSAttributedString(string: " о ", attributes: plainTextStyle))
            linkTextStyle[.link] = "shikiapp://linked/" + String(linked.id)
            fullNewsModel.headerAuthorAndLinked.append(NSAttributedString(string: linked.russian, attributes: linkTextStyle))
        } else if linked.name != "" {
            fullNewsModel.headerAuthorAndLinked.append(NSAttributedString(string: " о ", attributes: plainTextStyle))
            linkTextStyle[.link] = "shikiapp://linked/" + String(linked.id)
            fullNewsModel.headerAuthorAndLinked.append(NSAttributedString(string: linked.name, attributes: linkTextStyle))
        }
    }
    
    var clearBody = newsTopicRealm.body
    if let newsSource = clearBody.slice(from: "[source]", to: "[/source]") {
        if let newsSourceHost = URL(string: newsSource)?.host {
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.right

            linkTextStyle[.link] = newsSource
            linkTextStyle[.paragraphStyle] = style
            fullNewsModel.footerSource = NSAttributedString(string: newsSourceHost, attributes: linkTextStyle)
        }
        clearBody = String(clearBody.dropLast(("[source]" + newsSource + "[/source]").count))
    }
    
    let attributedNewsTopicBody = NSMutableAttributedString(string: clearBody, attributes: [.font: UIFont.systemFont(ofSize: 16)])
    let formattedBody = attributedNewsTopicBody.formateBBCodedString()
    fullNewsModel.body.append(formattedBody)
    
    
    
//    var attachmentsLink = Array(newsTopicRealm.htmlFooterImage)
//    let attachmentsFileNames = attachmentsLink.compactMap({$0.split(separator: "/").last})
//    let attachmentsFileNamesDuplicates = attachmentsFileNames.filter({ fileName in attachmentsFileNames.filter({$0 == fileName}).count > 1 })
//
//    if attachmentsFileNamesDuplicates.count != 0 {
//        attachmentsFileNamesDuplicates.forEach { (duplicateFileName) in
//            if let duplicateFilePreview = attachmentsLink.filter({$0.contains(duplicateFileName) && $0.contains("preview")}).first {
//                attachmentsLink.removeAll(where: {$0 == duplicateFilePreview})
//            }
//        }
//    }
//
//    attachmentsLink.forEach { (attachmentLink) in
//        fullNewsModel.footerAttachmentsLinks.append(("image", attachmentLink))
//    }
    
    fullNewsModel.footerAttachmentsImages = Array(newsTopicRealm.footerImages)
    fullNewsModel.footerAttachmentsVideos = Array(newsTopicRealm.footerVideos)
    
    fullNewsLoadingsIsComplieted()
}

func initFullNewsCommentsViewModel(_ topicCommentRealm: CommentRealm) {
    let font = UIFont.systemFont(ofSize: 14)
    var linkTextStyle: [NSAttributedString.Key: Any] = [
        .font: font,
    ]
    
    let plainTextStyle: [NSAttributedString.Key: Any] = [
       .font: UIFont.systemFont(ofSize: 16),
       .foregroundColor: UIColor(named: "PlainTextColor") ?? UIColor.black,
    ]
    
    let fullNewsCommentModel = FullNewsViewCommentModel()
    
    fullNewsCommentModel.id = topicCommentRealm.id
    
    if let user = newsUsersRealm.first(where: {$0.id == topicCommentRealm.userId}) {
        linkTextStyle[.link] = "shikiapp://author/" + String(user.id)
        fullNewsCommentModel.authorNickname.append(NSAttributedString(string: user.nickname, attributes: linkTextStyle))
        
        fullNewsCommentModel.authorAvatar = user.avatar
    }
    
    fullNewsCommentModel.createdDate = formateCreatedAtDateToString(topicCommentRealm.createdAt)
    
    let attributedCommentBody = NSMutableAttributedString(string: topicCommentRealm.body, attributes: plainTextStyle)
    let formattedBody = attributedCommentBody.formateBBCodedString()
    fullNewsCommentModel.comment = formattedBody
    
    fullNewsCommentsModel.append(fullNewsCommentModel)
    fullNewsCommentLoadingIsComplieted()
}

func clearFullNewsViewModel() {
    fullNewsCommentsRealm = []
    fullNewsModel = FullNewsViewModel()
    fullNewsCommentsModel = []
    recievedCommentsCount = 0
}

private func fullNewsLoadingsIsComplieted() {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fullNewsViewModelIsLoaded"), object: nil)
}

private func fullNewsCommentLoadingIsComplieted() {
    if fullNewsCommentsModel.count == recievedCommentsCount {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fullNewsViewModelIsLoaded"), object: nil)
    }
}
