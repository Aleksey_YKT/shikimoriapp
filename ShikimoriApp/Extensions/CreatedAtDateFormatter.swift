//
//  CreatedAtDateFormatter.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 16/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation

func formateCreatedAtDateToString(_ createdAt: Date) -> String {
    stringFormatter.locale = Locale(identifier: "ru_RU")
    stringFormatter.dateFormat = "dd.MM.yyyy"

    var createdAtString = ""
    if createdAt < Date(timeIntervalSinceNow: TimeInterval(-60 * 60 * 24 * 30)) {
        createdAtString = stringFormatter.string(from: createdAt)
    } else if createdAt > Date(timeIntervalSinceNow: TimeInterval(-60 * 60 * 24)) {
        if createdAt > Date(timeIntervalSinceNow: -60 * 60) {
            if let minutesAgo = (Date() - createdAt).minute {
                createdAtString = timeToStringFormatter(time: minutesAgo, type: 2)
            }
        } else {
            if let hoursAgo = (Date() - createdAt).hour {
                createdAtString = timeToStringFormatter(time: hoursAgo, type: 1)
            }
        }
    } else {
        if let daysAgo = (Date() - createdAt).day {
            createdAtString = timeToStringFormatter(time: daysAgo, type: 0)
        }
    }

    return createdAtString
}

private func timeToStringFormatter(time: Int, type: Int) -> String {
    var result = String(time)
    var timeTypeVariations: [String] = []
    switch type {
    case 0:
        timeTypeVariations = ["день назад", " день назад", " дня назад", " дней назад"]
    case 1:
        timeTypeVariations = ["час назад", " час назад", " часа назад", " часов назад"]
    case 2:
        timeTypeVariations = ["минуту назад", " минуту назад", " минуты назад", " минут назад"]
    default:
        return ""
    }
    
    if time % 10 == 1 && time != 11 {
        result = time == 1 ? timeTypeVariations[0] : result + timeTypeVariations[1]
    } else if time % 10 > 1 && time % 10 < 5 && (time > 21 || time < 11) {
        result += timeTypeVariations[2]
    } else if type == 2 && time == 0 {
        result = "сейчас"
    } else {
        result += timeTypeVariations[3]
    }
    
    return result
}

extension Date {
    static func -(recent: Date, previous: Date) -> (month: Int?, day: Int?, hour: Int?, minute: Int?, second: Int?) {
        let day = Calendar.current.dateComponents([.day], from: previous, to: recent).day
        let month = Calendar.current.dateComponents([.month], from: previous, to: recent).month
        let hour = Calendar.current.dateComponents([.hour], from: previous, to: recent).hour
        let minute = Calendar.current.dateComponents([.minute], from: previous, to: recent).minute
        let second = Calendar.current.dateComponents([.second], from: previous, to: recent).second

        return (month: month, day: day, hour: hour, minute: minute, second: second)
    }
}
