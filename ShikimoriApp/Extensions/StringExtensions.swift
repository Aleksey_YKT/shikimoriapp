//
//  StringExtensions.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 19/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation

extension String {
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}
