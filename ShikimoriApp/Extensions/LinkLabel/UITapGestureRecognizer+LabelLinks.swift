//
//  UITapGestureRecognizer+LabelLinks.swift
//  TwIM
//
//  Created by Andrew Hart on 06/08/2015.
//  Copyright (c) 2015 Project Dent. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

extension UIGestureRecognizer {
    //Returns a character that was touched, or nil if none.
    func indexOfCharacterTouched(label: UILabel) -> Int? {
        guard let attributedText = label.attributedText else {
            return nil
        }
        
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer()
        let textStorage = NSTextStorage(attributedString: attributedText)
        textStorage.addLayoutManager(layoutManager)
        
        textContainer.lineFragmentPadding = 0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        textContainer.size = label.bounds.size
        
        layoutManager.addTextContainer(textContainer)

        let locationOfTouchInLabel = self.location(in: label)

        let indexOfCharacter = layoutManager.characterIndex(
            for: locationOfTouchInLabel,
            in: textContainer,
            fractionOfDistanceBetweenInsertionPoints: nil)
        
        return indexOfCharacter
    }
}
