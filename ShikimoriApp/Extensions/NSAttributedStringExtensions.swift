//
//  NSAttributedStringExtensions.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 21/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import UIKit

private class ExtendedBBCode {
    var code: String = ""
    var value: String = ""
    var beginingLocation: NSRange? = nil
    var endingLocation: NSRange? = nil
}

extension String {
    var urlQueryAllowedLink: String {
        self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
}

var n = 1

extension NSMutableAttributedString {
    func formateBBCodedString() -> NSMutableAttributedString {
        if let detector = try? NSRegularExpression(pattern: "\\[(.*?)\\]", options: []) {
            let matches = detector.matches(in: self.string, options: [], range: NSRange(location: 0, length: self.length))
            
            var extendedBBCode: [ExtendedBBCode] = []
            for match in matches {
                guard let range = Range(match.range, in: self.string) else { continue }
                let bbcode = self.string[range]
                
                let clearCode = bbcode.dropLast().dropFirst()
                if clearCode.first != "/" {
                    let newExtendedBBCode = ExtendedBBCode()
                    newExtendedBBCode.beginingLocation = self.string.nsRange(from: range)
                    
                    if clearCode.split(separator: "=").count > 1 {
                        newExtendedBBCode.code = String(clearCode.split(separator: "=")[0])
                        newExtendedBBCode.value = String(clearCode.dropFirst(newExtendedBBCode.code.count + 1))
                    } else {
                        newExtendedBBCode.code = String(clearCode)
                    }
                    
                    extendedBBCode.append(newExtendedBBCode)
                } else {
                    let endingCode = String(clearCode.dropFirst())
                    if let beginningOfCode = extendedBBCode.last(where: {$0.code == endingCode && $0.endingLocation == nil}) {
                        beginningOfCode.endingLocation = self.string.nsRange(from: range)
                    }
                }
            }
            
            extendedBBCode.forEach { (bbcode) in
                if let beginningRange = bbcode.beginingLocation, let endingRange = bbcode.endingLocation {
                    let range = NSRange(location: beginningRange.lowerBound, length: endingRange.upperBound - beginningRange.lowerBound)
                    var textStyle: [NSAttributedString.Key: Any] = [:]
                    var oldTextStyle: [NSAttributedString.Key: Any] = [:]
                    self.enumerateAttributes(in: range, options: []) { (attr, range, stop) in
                        oldTextStyle = attr
                    }
                    
                    if bbcode.value.count > 0 {
                        switch bbcode.code {
                        case "size":
                            let size = (Int(bbcode.value) ?? 16) > 24 ? 24 : Int(bbcode.value) ?? 16
                            textStyle = [.font: UIFont.systemFont(ofSize: CGFloat(size))]
                        case "quote":
                            textStyle = [.font: UIFont.systemFont(ofSize: 16).italic(), .backgroundColor: UIColor(named: "QuotedTextBackgroundColor") ?? UIColor.lightGray, .kern: 0.4]
                        case "url":
                            if let linkURL = URL(string: bbcode.value.urlQueryAllowedLink) {
                                textStyle = [.link: linkURL]
                            }
                        case "color":
                            if let color = colorWithName(bbcode.value) {
                                textStyle = [.foregroundColor: color]
                            }
                        case "person", "anime", "manga", "character", "ranobe":
                            if let linkURL = URL(string: appURL + bbcode.code + "/" + bbcode.value) {
                                textStyle = [.link: linkURL]
                            }
                        case "comment":
                            if let linkURL = URL(string: appURL + bbcode.code + "/" + bbcode.value) {
                                textStyle = [.link: linkURL]
                            }
                        default:
                            return
//                            print(bbcode.code, attributedSubstring(from: range))
                        }
                    } else {
                        switch bbcode.code {
                        case "b":
                            textStyle = [.font: UIFont.systemFont(ofSize: 16).bold()]
                        case "i":
                            textStyle = [.font: UIFont.systemFont(ofSize: 16).italic()]
                        case "quote":
                            textStyle = [.font: UIFont.systemFont(ofSize: 16).italic(), .backgroundColor: UIColor(named: "QuotedTextBackgroundColor") ?? UIColor.lightGray, .kern: 0.4]
                        case "u":
                            textStyle = [.underlineStyle: NSUnderlineStyle.single.rawValue]
                        case "s":
                            textStyle = [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
                        case "left", "right", "center":
                            let paragraphStyle = NSMutableParagraphStyle()
                            switch bbcode.code {
                            case "right":
                                paragraphStyle.alignment = NSTextAlignment.right
                            case "center":
                                paragraphStyle.alignment = NSTextAlignment.center
                            default:
                                paragraphStyle.alignment = NSTextAlignment.left
                            }
                            textStyle = [.paragraphStyle: paragraphStyle]
                        case "wall", "list":
                            return
                        case "img":
                            if let linkURL = URL(string: self.attributedSubstring(from: NSRange(location: beginningRange.upperBound, length: endingRange.lowerBound - beginningRange.upperBound)).string.urlQueryAllowedLink) {
                                textStyle = [.link: linkURL]
                            }
                        default:
                            return
//                            print(bbcode.code, attributedSubstring(from: range))
                        }
                    }
            
                    if oldTextStyle.count != 0 {
                        if oldTextStyle.keys.contains(.font) && textStyle.keys.contains(.font) {
                            if let oldFont = oldTextStyle[.font] as? UIFont, let newFont = textStyle[.font] as? UIFont {
                                var desiredFontSize: CGFloat = 16.0
                                var desiredFont = UIFont.systemFont(ofSize: desiredFontSize)
                                if (oldFont.fontName != newFont.fontName) {
                                    if oldFont.fontName == desiredFont.fontName {
                                        desiredFont = newFont
                                    } else {
                                        if newFont.fontName == desiredFont.fontName {
                                            desiredFont = oldFont
                                        } else {
                                            desiredFont = UIFont.systemFont(ofSize: desiredFontSize).boldItalic()
                                        }
                                    }
                                }
                                
                                if (oldFont.pointSize != newFont.pointSize) {
                                    desiredFontSize = newFont.pointSize != desiredFontSize ? newFont.pointSize : oldFont.pointSize
                                    desiredFont = desiredFont.setSize(desiredFontSize)
                                }
                                textStyle = [.font: desiredFont]
                            }
                        }
                    }
                    
                    self.addAttributes(textStyle, range: range)
                }
            }
        }
        
        
        if let detector = try? NSRegularExpression(pattern: "\\[(.*?)\\]", options: []) {
            var location = 0
            while let match = detector.firstMatch(in: self.string, options: [], range: NSRange(location: location, length: self.length - location)) {
                location = removeBBCode(match, location: location)
            }
        }
        
        return self
    }
    
    private func removeBBCode(_ match: NSTextCheckingResult, location: Int) -> Int {
        guard let range = Range(match.range, in: self.string) else { return self.length }
        let nsRange = self.string.nsRange(from: range)
        let code = String(self.string[range])
        
        if code == "[*]" {
            self.replaceCharacters(in: nsRange, with: "\n")
        } else if code.contains("quote") {
            if code.contains("=") {
                if code.contains(";") {
                    let quoted = String(code.split(separator: "=")[1].dropLast().split(separator: ";")[2])
                    if let linkURL = URL(string: appURL + "comment/" + String(code.split(separator: "=")[1].dropLast().split(separator: ";")[0].dropFirst())) {
                        let quoteSign = location == 0 ? NSMutableAttributedString(string: "❝ ") : NSMutableAttributedString(string: "\n❝ ")
                        let string = NSAttributedString(string: "@" + quoted + "\n", attributes: [.link: linkURL])
                        quoteSign.append(string)
                        self.replaceCharacters(in: nsRange, with: quoteSign)
                    } else {
                        print("incorrect ", code)
                        self.deleteCharacters(in: nsRange)
                    }
                } else {
                    let quoted = String(code.split(separator: "=")[1].dropLast())
                    if let linkURL = URL(string: appURL + "user/" + quoted.urlQueryAllowedLink) {
                        let quoteSign = NSMutableAttributedString(string: "\n❝ ")
                        let string = NSAttributedString(string: quoted + "\n", attributes: [.link: linkURL])
                        quoteSign.append(string)
                        self.replaceCharacters(in: nsRange, with: quoteSign)
                    } else {
                        print("incorrect ", code)
                        self.deleteCharacters(in: nsRange)
                    }
                }
            } else if !code.contains("/") {
                let string = NSAttributedString(string: "❝\n", attributes: [:])
                self.replaceCharacters(in: nsRange, with: string)
            } else if code.contains("/") {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = NSTextAlignment.right
                let string = NSAttributedString(string: "\n❞", attributes: [.paragraphStyle: paragraphStyle])
                self.replaceCharacters(in: nsRange, with: string)
            } else {
                print("incorrect ", code)
                self.deleteCharacters(in: nsRange)
            }
        } else if code.contains("comment") {
            if !code.contains("/") {
                if let linkURL = URL(string: appURL + "comment" + "/" + code.split(separator: "=")[1].dropLast()) {
                    let string = NSAttributedString(string: "@", attributes: [.link: linkURL])
                    self.replaceCharacters(in: nsRange, with: string)
                }
            } else {
                self.deleteCharacters(in: nsRange)
            }
        } else if code.contains("replies") {
            let replies = code.split(separator: "=")[1].dropLast().split(separator: ",")
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = NSTextAlignment.right
            let repilesString = NSMutableAttributedString(string: "Ответы: ", attributes: [.paragraphStyle: paragraphStyle])
            for (i, reply) in replies.enumerated() {
                var replier = ""
                if let replyAuthor = returnFullNewsCommentAuthor(messageIdString: String(reply)) {
                    replier = replyAuthor
                } else {
                    replier = String(reply)
                }
                
                if let linkURL = URL(string: appURL + "comment" + "/" + reply) {
                    repilesString.append(NSAttributedString(string: "@" + String(replier), attributes: [.link: linkURL]))
                    if i != replies.count - 1 {
                        repilesString.append(NSAttributedString(string: ", "))
                    }
                }
            }
            self.replaceCharacters(in: nsRange, with: repilesString)
        } else {
            self.deleteCharacters(in: nsRange)
        }
            
        return(nsRange.location)
    }
}

extension UIFont {
    func withTraits(traits: UIFontDescriptor.SymbolicTraits...) -> UIFont {
        guard let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits).union(self.fontDescriptor.symbolicTraits)) else {
            return self
        }
        return UIFont(descriptor: descriptor, size: 0)
    }

    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }

    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
    
    func boldItalic() -> UIFont {
        return withTraits(traits: .traitBold, .traitItalic)
    }
    
    func setSize(_ size: CGFloat) -> UIFont {
        return UIFont(descriptor: self.fontDescriptor, size: size)
    }
}

extension StringProtocol {
    func nsRange(from range: Range<Index>) -> NSRange {
        return .init(range, in: self)
    }
}

private func colorWithName(_ name: String) -> UIColor? {
    switch name {
    case "black": return UIColor.black
    case "darkGray": return UIColor.darkGray
    case "lightGray": return UIColor.lightGray
    case "white": return UIColor.white
    case "gray": return UIColor.gray
    case "red": return UIColor.red
    case "green": return UIColor.green
    case "blue": return UIColor.blue
    case "cyan": return UIColor.cyan
    case "yellow": return UIColor.yellow
    case "magenta": return UIColor.magenta
    case "orange": return UIColor.orange
    case "purple": return UIColor.purple
    case "brown": return UIColor.brown
    default: return nil
    }
}
