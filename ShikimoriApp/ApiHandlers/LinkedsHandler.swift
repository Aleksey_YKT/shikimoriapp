//
//  LinkedsHandler.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

func parseTopicLinkedsJSON(_ json: JSON) {
    let linkedRealm = LinkedRealm()
    
//    func parse(_ realm: Realm) {
    if let id = json["id"].int {
        linkedRealm.id = id
    }
    
    if let name = json["name"].string {
        linkedRealm.name = name
    }
    
    if let russian = json["russian"].string {
        linkedRealm.russian = russian
    }
    
    if let imageOriginal = json["image"]["original"].string {
        linkedRealm.imageOriginal = imageOriginal
    }
    
    if let imagePreview = json["image"]["preview"].string {
        linkedRealm.imagePreview = imagePreview
    }
    
    if let url = json["url"].string {
        linkedRealm.url = url
    }
    
    if let kind = json["kind"].string {
        linkedRealm.kind = kind
    }
    
    newsLinkedsRealm.append(linkedRealm)
//        realm.add(linkedRealm, update: .modified)
//    }
//    
//    if let realm = try? Realm() {
//        if realm.isInWriteTransaction {
//            parse(realm)
//        } else {
//            try? realm.write {
//                parse(realm)
//            }
//        }
//    }
}
