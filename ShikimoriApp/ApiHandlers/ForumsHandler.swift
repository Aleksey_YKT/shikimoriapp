//
//  ForumsHandler.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import SwiftyJSON

func parseTopicForumJSON(_ json: JSON) {
    let forumRealm = ForumRealm()
    
//    func parse(_ realm: Realm) {
    if let id = json["id"].int {
        forumRealm.id = id
    }
    
    if let name = json["name"].string {
        forumRealm.name = name
    }
    
    if let permalink = json["permalink"].string {
        forumRealm.permalink = permalink
    }
    
    if let url = json["url"].string {
        forumRealm.url = url
    }
    
    if let position = json["position"].int {
        forumRealm.position = position
    }
    
    newsForumsRealm.append(forumRealm)
//        realm.add(forumRealm, update: .modified)
//    }
    
//    if let realm = try? Realm() {
//        if realm.isInWriteTransaction {
//            parse(realm)
//        } else {
//            try? realm.write {
//                parse(realm)
//            }
//        }
//    }
}
