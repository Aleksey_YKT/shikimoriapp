//
//  UsersHandler.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import SwiftyJSON

func parseTopicUserJSON(_ json: JSON) {
    let userRealm = UserRealm()
    
//    func parse(_ realm: Realm) {
    if let id = json["id"].int {
        userRealm.id = id
    }
    
    if let nickname = json["nickname"].string {
        userRealm.nickname = nickname
    }
    
    if let avatar = json["image"]["x160"].string {
        userRealm.avatar = avatar
    }
    
    if let lastOnlineAt = json["last_online_at"].string {
        if let date = dateFormatter.date(from: lastOnlineAt) {
            userRealm.lastOnlineAt = date
        }
    }
    
    newsUsersRealm.append(userRealm)
//        realm.add(userRealm, update: .modified)
//    }
//
//    if let realm = try? Realm() {
//        if realm.isInWriteTransaction {
//            parse(realm)
//        } else {
//            try? realm.write {
//                parse(realm)
//            }
//        }
//    }
}
