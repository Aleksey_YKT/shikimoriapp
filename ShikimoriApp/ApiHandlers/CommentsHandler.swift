//
//  CommentsHandler.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 24/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import SwiftyJSON

let newsCommentsLimit = 20
var currentCommentsPage = 1
var newsCommentsHasNextPage = true
var recievedCommentsCount = 0

func getNewsTopicComments(page: Int, commentableId: Int) {
    Alamofire.request(getTopicComments, parameters: ["commentable_id": commentableId, "commentable_type": "Topic", "page": page, "limit": newsCommentsLimit], headers: nil)
        .validate(statusCode: 200..<300)
        .responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonArray = JSON(value)
                newsCommentsHasNextPage = jsonArray.count != newsCommentsLimit
                recievedCommentsCount += newsCommentsHasNextPage ? (recievedCommentsCount == 0 ? jsonArray.count : jsonArray.count - 1) : jsonArray.count
                
                jsonArray.forEach({ (_, json) in
                    parseGetNewsTopicsCommentsJSON(json)
                })
            case .failure(let error):
                print(error)
            }
    }
}

private func parseGetNewsTopicsCommentsJSON(_ json: JSON) {
    let topicCommentRealm = CommentRealm()
    
//    func parse(_ realm: Realm) {
    if let id = json["id"].int {
        topicCommentRealm.id = id
    }
    
    if let htmlBody = json["html_body"].string {
        topicCommentRealm.htmlBody = htmlBody
    }
    
    if let body = json["body"].string {
        topicCommentRealm.body = body
    }
    
    if let userId = json["user"]["id"].int {
        topicCommentRealm.userId = userId
        
        parseTopicUserJSON(json["user"])
    }
    
    if let createdAt = json["created_at"].string {
        if let date = dateFormatter.date(from: createdAt) {
            topicCommentRealm.createdAt = date
        }
    }
    
    if let updatedAt = json["updated_at"].string {
        if let date = dateFormatter.date(from: updatedAt) {
            topicCommentRealm.updatedAt = date
        }
    }
    
    if let isSummary = json["is_summary"].bool {
         topicCommentRealm.isSummary = isSummary
    }
    
    if let commentableType = json["commentable_type"].string {
        topicCommentRealm.commentableType = commentableType
    }
    
    if let canBeEdited = json["can_be_edited"].bool {
        topicCommentRealm.canBeEdited = canBeEdited
    }
    
    if let commentableId = json["commentable_id"].int {
        topicCommentRealm.commentableId = commentableId
    }
    
    if let isOfftopic = json["is_offtopic"].bool {
        topicCommentRealm.isOfftopic = isOfftopic
    }
    
    if fullNewsCommentsRealm.filter({$0.id == topicCommentRealm.id}).count == 0 {
        fullNewsCommentsRealm.append(topicCommentRealm)
        initFullNewsCommentsViewModel(topicCommentRealm)
    }
//        realm.add(topicRealm, update: .modified)
//    }
    
//    if let realm = try? Realm() {
//        if realm.isInWriteTransaction {
//            parse(realm)
//        } else {
//            try? realm.write {
//                parse(realm)
//            }
//        }
//    }
}

func returnFullNewsCommentAuthor(messageIdInt: Int = -1, messageIdString: String = "") -> String? {
    let messageId = messageIdInt != -1 ? messageIdInt : (Int(messageIdString) ?? -1)
    
    if messageId != -1 {
        if let fullNewsComment = fullNewsCommentsRealm.first(where: {$0.id == messageId}) {
            if let fullNewsCommentAuthor = newsUsersRealm.first(where: {$0.id == fullNewsComment.userId}) {
                return fullNewsCommentAuthor.nickname
            }
        }
    }
    
    return nil
}
