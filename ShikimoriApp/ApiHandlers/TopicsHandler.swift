//
//  TopicsHandler.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import SwiftyJSON
import Kanna

let newsTopicsLimit = 20
var currentNewsPage = 1
var newsTopicsHasNextPage = true
var recievedNewsTopicsCount = 0

func getNewsTopics(page: Int) {    
    Alamofire.request(getTopicsURL, parameters: ["page": page, "limit": newsTopicsLimit, "forum": "news"], headers: nil)
        .validate(statusCode: 200..<300)
        .responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonArray = JSON(value)
                newsTopicsHasNextPage = jsonArray.count != newsTopicsLimit
                recievedNewsTopicsCount += newsTopicsHasNextPage ? (recievedNewsTopicsCount == 0 ? jsonArray.count : jsonArray.count - 1) : jsonArray.count

                jsonArray.forEach({ (_, json) in
                    parseGetTopicsJSON(json)
                })
            case .failure(let error):
                print(error)
            }
    }
}

private func parseGetTopicsJSON(_ json: JSON) {
        
    let topicRealm = TopicRealm()

//    func parse(_ realm: Realm) {
    if let id = json["id"].int {
        topicRealm.id = id
    }
    
    if newsTopicsRealm.filter({$0.id == topicRealm.id}).count == 0 {
        if let linkedType = json["linked_type"].string {
            topicRealm.linkedType = linkedType
        }
        
        if let htmlFooter = json["html_footer"].string {
            topicRealm.htmlFooter = htmlFooter
            
            if let html = try? HTML(html: htmlFooter, encoding: String.Encoding.utf8) {
                if let htmlFooterImages = parseFooterHTMLImages(html: html) {
                    topicRealm.footerImages.append(objectsIn: htmlFooterImages)
                }
            
                if let htmlFooterVideos = parseFooterHTMLVideos(html: html) {
                    topicRealm.footerVideos.append(objectsIn: htmlFooterVideos)
                }
            }
        }
        
        if let topicTitle = json["topic_title"].string {
            topicRealm.topicTitle = topicTitle
        }
        
        if let htmlBody = json["html_body"].string {
            topicRealm.htmlBody = htmlBody
        }
        
        if let commentsCount = json["comments_count"].int {
            topicRealm.commentsCount = commentsCount
        }
        
        if let body = json["body"].string {
            topicRealm.body = body
        }
        
        if let viewed = json["viewed"].bool {
            topicRealm.viewed = viewed
        }
        
        if let episode = json["episode"].int {
            topicRealm.episode = episode
        }
        
        if let linkedId = json["linked_id"].int {
            topicRealm.linkedId = linkedId
            
            parseTopicLinkedsJSON(json["linked"])
        }
        
        if let lastCommentViewed = json["last_comment_viewed"].bool {
            topicRealm.lastCommentViewed = lastCommentViewed
        }
        
        if let event = json["event"].string {
            topicRealm.event = event
        }
        
        if let createdAt = json["created_at"].string {
            if let date = dateFormatter.date(from: createdAt) {
                topicRealm.createdAt = date
            }
        }
        
        if let type = json["type"].string {
            topicRealm.type = type
        }
        
        if let userId = json["user"]["id"].int {
            topicRealm.userId = userId
            
            parseTopicUserJSON(json["user"])
        }
        
        if let forumId = json["forum"]["id"].int {
            topicRealm.forumId = forumId
            
            parseTopicForumJSON(json["forum"])
        }
    
        newsTopicsRealm.append(topicRealm)
        initNewsTableViewModel(topicRealm)
//        realm.add(topicRealm, update: .modified)
//    }
    }
    
//    if let realm = try? Realm() {
//        if realm.isInWriteTransaction {
//            parse(realm)
//        } else {
//            try? realm.write {
//                parse(realm)
//            }
//        }
//    }
}

func parseFooterHTMLImages(html: HTMLDocument) -> [String]? {
    var links: [String] = []
    
    for item in html.xpath("//a") {
        if let itemClass = item["class"] {
            if itemClass.contains("image") {
                if let link = item["href"] {
                    links.append(link)
                }
            }
        }
    }
    
    return links.count != 0 ? links : nil
}

func parseFooterHTMLVideos(html: HTMLDocument) -> [TopicVideoRealm]? {
    var links: [TopicVideoRealm] = []
    
    for item in html.xpath("//a") {
        let video = TopicVideoRealm()
        
        if let itemClass = item["class"] {
            if itemClass.contains("video") {
                if let link = item["href"] {
                    video.link = link
                }
                
                if let itemChilds = item.at_xpath("//img") {
                    if let itemChildsPreview = itemChilds["src"] {
                        video.preview = itemChildsPreview.contains("//img.youtube.com/") ? "https:" + itemChildsPreview : itemChildsPreview
                    }
                }
            
                links.append(video)
            }
        }
    }
    
    return links.count != 0 ? links : nil
}
