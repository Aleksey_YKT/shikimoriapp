//
//  ForumsRealm.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

class ForumRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var position: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var permalink: String = ""
    @objc dynamic var url: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
