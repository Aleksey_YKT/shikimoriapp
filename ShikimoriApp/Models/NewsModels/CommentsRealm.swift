//
//  CommentsRealm.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 25/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

class CommentRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var body: String = ""
    @objc dynamic var htmlBody: String = ""
    @objc dynamic var userId: Int = 0
    @objc dynamic var createdAt: Date = Date()
    @objc dynamic var updatedAt: Date = Date()
    @objc dynamic var isSummary: Bool = false
    @objc dynamic var commentableType: String = ""
    @objc dynamic var canBeEdited: Bool = false
    @objc dynamic var commentableId: Int = 0
    @objc dynamic var isOfftopic: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
