//
//  UsersRealm.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

class UserRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var nickname: String = ""
    @objc dynamic var avatar: String = ""
    @objc dynamic var lastOnlineAt: Date = Date()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
