//
//  LinkedRealm.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

class LinkedRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var russian: String = ""
    @objc dynamic var imagePreview: String = ""
    @objc dynamic var imageOriginal: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var kind: String = ""
    @objc dynamic var status: String = ""
    @objc dynamic var episodes: Int = 0
    @objc dynamic var episodesAired: Int = 0
    @objc dynamic var airedOn: Bool = false
    @objc dynamic var releasedOn: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
