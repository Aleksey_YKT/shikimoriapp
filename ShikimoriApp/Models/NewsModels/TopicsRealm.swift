//
//  TopicsRealm.swift
//  ShikimoriApp
//
//  Created by Aleksey Ivanov on 14/09/2019.
//  Copyright © 2019 Aleksey Ivanov. All rights reserved.
//

import Foundation
import RealmSwift

class TopicVideoRealm: Object {
    @objc dynamic var preview: String = ""
    @objc dynamic var link: String = ""
}

class TopicRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var topicTitle: String = ""
    @objc dynamic var body: String = ""
    @objc dynamic var htmlBody: String = ""
    @objc dynamic var htmlFooter: String = ""
    let footerImages = List<String>()
    let footerVideos = List<TopicVideoRealm>()
    @objc dynamic var createdAt: Date = Date()
    @objc dynamic var commentsCount: Int = 0
    @objc dynamic var forumId: Int = 0
    @objc dynamic var userId: Int = 0
    @objc dynamic var type: String = ""
    @objc dynamic var linkedId: Int = 0
    @objc dynamic var linkedType: String = ""
    @objc dynamic var viewed: Bool = false
    @objc dynamic var lastCommentViewed: Bool = false
    @objc dynamic var event: String = ""
    @objc dynamic var episode: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
